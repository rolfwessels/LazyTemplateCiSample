/* service.faction */

angular.module('webapp.services')
	.service('service.faction', ['$log', 'service.endPoint',
		function ($log, serviceEndPoint) {

			/* 
             * Service
             */
		    var factionHub = {};
		    var returnService = serviceEndPoint('Faction', factionHub);

		    return returnService;

		}
	]);