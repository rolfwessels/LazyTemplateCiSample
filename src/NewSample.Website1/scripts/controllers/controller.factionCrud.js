/* controller.factionCrud */

angular.module('webapp.controllers')
    .controller('controller.factionCrud', ['$scope', 'service.crud', 'service.faction',
        function ($scope, crudService, serviceFaction) {

            $scope.crudFaction = crudService(serviceFaction);

        }
    ]);