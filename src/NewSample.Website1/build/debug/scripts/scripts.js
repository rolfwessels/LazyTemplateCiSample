angular
    .module('webapp', [
		'angular-loading-bar',
		'ngAnimate',
        'ui.materialize',
        'webapp.routes',
        'webapp.controllers',
        'webapp.directives',
        'webapp.filters',
        'webapp.services'
    ])
    .value('tokenUrl', 'http://localhost:8081/token')
    .value('apiUrlBase', 'http://localhost:8081/api')
    .value('signalrBase', 'http://localhost:8081/signalr')    
    .config(function() {
        $(".button-collapse").sideNav({
          menuWidth: 300, // Default is 240
          edge: 'left', // Choose the horizontal origin
          closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });
    })
    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
      cfpLoadingBarProvider.includeSpinner = false;
    }])
    .run(['service.authorization`', function(authorizationService) {
      authorizationService.isAuthenticatedOrRedirect();
    }]);

/* Controllers */

angular.module('webapp.controllers', []);
/* Directives */

angular.module('webapp.directives', []);

/* Filters */

angular.module('webapp.filters', []);

/* routes */

angular
	.module('webapp.routes', ['ngRoute'])
	.config(['$routeProvider', function($routeProvider) {

        $routeProvider
        .when('/', {
              templateUrl: 'views/dashboard.html',
              controller: 'controller.dashboard'
        })
        .when('/user/:id?', {
            templateUrl: 'views/user.html',
            controller: 'controller.userCrud'
        })
        .when('/project/:id?', {
            templateUrl: 'views/project.html',
            controller: 'controller.projectCrud'
        })
        .when('/login/', {
              templateUrl: 'views/login.html',
              controller: 'loginCtrl'
            })
        .when('/forgotPassword', {
              templateUrl: 'views/forgotPassword.html',
              controller: 'forgotPasswordCtrl'
            })
        .otherwise({ redirectTo: '/' });

      }
    ]);
/* Services */

angular.module('webapp.services', ['LocalStorageModule']);

/* controller.dashboard */

angular.module('webapp.controllers')
    .controller('controller.dashboard', ['$scope', '$log', 'service.message', 'service.project', 'service.user',
        function ($scope, $log,  messageService, serviceProject, serviceUser) {

            $scope.allCounter = [];
            mapData(serviceUser, 'users', '#/user');
            mapData(serviceProject, 'projects', '#/project');
            
            function mapData(service,onScope,link) {
                var counter = { name: onScope, items: [], count: 0 , link : link};
                $scope.allCounter.push(counter);
                service.onUpdate($scope, function (call) {
                    $scope.$apply(function() {
                        if (call.updateType == 0) {
                            counter.count += 1;
                        } else if (call.updateType == 2) {
                            counter.count += -1;
                        }
                    });
                });
                update(service, counter);
            }

            function update(service, counter) {
                service.getAllPaged('$top=5').then(function (data) {
                    counter.count = data.count;
                    counter.items = data.items;
                }, messageService.error, messageService.debug);
            }
        }
    ]);

/* controller.projectCrud */

angular.module('webapp.controllers')
    .controller('controller.projectCrud', ['$scope', 'service.crud', 'service.project',
        function ($scope, crudService, serviceProject) {

            $scope.crudProject = crudService(serviceProject);

        }
    ]);

    /* scaffolding [
        {
          "FileName": "route.js",
          "Indexline": ".otherwise",
          "InsertAbove": true,
          "InsertInline": false,
          "Lines": [
            ".when('/project/:id?', {",
            "    templateUrl: 'views/project.html',",
            "    controller: 'controller.projectCrud'",
            "})"
          ]
        },
        {
          "FileName": "controller.dashboard.js",
          "Indexline": "mapData(serviceProject",
          "InsertAbove": true,
          "InsertInline": false,
          "Lines": [
            "mapData(serviceProject, 'projects', '#/project');"
          ]
        },
        {
          "FileName": "controller.dashboard.js",
          "Indexline": ", 'service.project'",
          "InsertAbove": false,
          "InsertInline": true,
          "Lines": [
            ", 'service.project'"
          ]
        },
        {
          "FileName": "controller.dashboard.js",
          "Indexline": ", serviceProject",
          "InsertAbove": false,
          "InsertInline": true,
          "Lines": [
            ", serviceProject"
          ]
        }
    ] scaffolding */

/* controller.user */

angular.module('webapp.controllers')
    .controller('controller.userCrud', ['$scope', 'service.crud', 'service.user',
        function($scope, serviceCrud, serviceUser) {   

            $scope.crudUser = serviceCrud(serviceUser);

        }
    ]);

/* dashboardCtrl */

angular.module('webapp.controllers')
    .controller('forgotPasswordCtrl', ['$scope', '$log', 'service.message', 'service.authorization`','$location',
        function($scope, $log, messageService, authorizationService,$location) {

            /*
             * Scope
             */
			var  currentUser = authorizationService.currentSession();

            $scope.model = { email: currentUser.email };
            $scope.forgotPassword = forgotPassword;
            
            /*
             * Functions
             */

            function forgotPassword() {
               var authenticate = authorizationService.forgotPassword($scope.model.email);
                authenticate.then(function() {
                	messageService.info('Your password has been sent to your email');
                    $location.path("/login");
                },messageService.error);

            }

        }
    ]);

/* dashboardCtrl */

angular.module('webapp.controllers')
    .controller('loginCtrl', ['$scope', '$log', 'service.message', 'service.authorization`','$location',
        function($scope, $log, messageService, authorizationService,$location) {

            /*
             * Scope
             */
            var  currentUser = authorizationService.currentSession();
            $scope.model = {
                email: currentUser.email||'asdf',
                password: ''
            };
            $scope.login = login;
            $scope.forgotPassword = forgotPassword;

            /*
             * Functions
             */
            function login() {
                
                var authenticate = authorizationService.authenticate($scope.model.email, $scope.model.password);
                authenticate.then(function() {
                    authorizationService.continueToPage();
                }, function(message) {
                	 $scope.model.password = "";
                    messageService.error(message||'Invalid username or password.');
                },messageService.info);

            }

            function forgotPassword() {
	            $location.path('forgotPassword');
            }

        }
    ]);

/* dashboardCtrl */

angular.module('webapp.controllers')
  .controller('navigationCtrl', ['$scope','$rootScope', 'service.authorization`', '$timeout', '$location',
		function ($scope,$rootScope, authorizationService, $timeout, $location) {
    
    
    $scope.logout = logout;
    $scope.navigateHome = navigateHome;
    $scope.login = login;
    $rootScope.$watch("isAuthenticated", function(newValue) {
      if (!newValue) {
          $timeout(function() {

          },500);           
        }
      }
    );
      
    function navigateHome() {
      $location.path("/");
    }

    function login() {
      $location.path("/login");
    }

    function logout() {
      authorizationService.logout();
    }
  }]);

/* messageService */

angular.module('webapp.directives')
    .directive('crudPanel', function() {
        return {
            restrict: 'E',
            scope: {
                data: '=data'
            },
            transclude: true,
            templateUrl: 'views/partial/crudPanelPartial.html',
            link: function ($scope) {
                $scope.data.subscribe($scope);
            }
        };
    })
    .directive('crudForm', function() {
        return {
            restrict: 'E',
            scope: {
                data: '=data'
            },
            transclude: true,
            templateUrl: 'views/partial/crudFormPartial.html'
            
        };
    })
    .directive('transpose', ['$log', function ($log) {
    return {
        restrict: 'EAC',
        link: function($scope, $element, $attrs, controller, $transclude) {
            if (!$transclude) {
                $log.error('Illegal use of ngTransclude directive in the template! ' +
                    'No parent directive that requires a transclusion found. ' +
                    'Element: ' + $element);
                return;
            }

            var iScopeType = $attrs.transpose || 'sibling';

            switch (iScopeType) {
            case 'sibling':
                $transclude(function(clone) {
                    $element.empty();
                    $element.append(clone);
                });
                break;
            case 'parent':
                $transclude($scope, function(clone) {
                    $element.empty();
                    $element.append(clone);
                });
                break;
            case 'child':
                var object = $scope.$new();
                var iChildScope = object;
                $transclude(iChildScope, function(clone) {
                    $element.empty();
                    $element.append(clone);
                    $element.on('$destroy', function() {
                        iChildScope.$destroy();
                    });
                });
                break;
            }
        }
    };
}]);

/* service.authorization */

angular.module('webapp.services')
	.service('service.authorization`', ['$log', '$http', 'localStorageService', 'tokenUrl', 'apiUrlBase', '$q', '$location', '$rootScope',
		function($log, $http, localStorageService, tokenUrl, apiUrlBase, $q, $location,$rootScope) {
			
			var clientId = "NewSample.Api";
			var currentSession = localStorageService.get('token') || {};
			var directTo = "/";
			var pathsToIgnore = ["/login",'/forgotPassword'];
			$rootScope.isAuthenticated = isAuthenticate();
			
			/*
			 * Private methods
			 */
			function saveSession() {
				localStorageService.set('token', currentSession);
				$rootScope.isAuthenticated = isAuthenticate();
			}

			function isAuthenticate() {
			    var isAuthed = currentSession.accessToken !== null && (new Date(currentSession.expires) > new Date());
			    console.log("isAuthenticate: " + isAuthed);
			    return isAuthed;    
			}

		    /* 
			 * Service
			 */
			return {
				currentSession : function() {
					return currentSession;
				},
				continueToPage : function() {
					$location.path(directTo);
				},
				isAuthenticatedOrRedirect: function() {
					if (pathsToIgnore.indexOf($location.path()) >= 0) {
						return;
					}
				    
					if (!this.isAuthenticate()) {
						directTo = $location.path();
						console.log(directTo);
						$location.path("/login");
					}
				},
				
				isAuthenticate: isAuthenticate,


				logout: function() {
					currentSession.accessToken = null;
					saveSession();
					$location.path("/login");
				},
				authenticate: function(email, password) {
					var deferred = $q.defer();

					var config = {
						method: 'POST',
						url: tokenUrl,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
						},
						data: 'client_id=' + clientId + '&grant_type=password&username=' + email + '&password=' + password,
					};

					$http(config)
						.success(function(data) {
							currentSession.expires = Date.parse(data['.expires']);
							currentSession.issued =  Date.parse(data['.issued']);
							currentSession.accessToken = data.access_token;
							currentSession.displayName = data.displayName;
							currentSession.permissions = data.permissions;
							currentSession.email = data.userName;
							saveSession();							
							deferred.resolve(data);
						})
						.error(function(data) {
							$log.error("Auth ERROR: ", data);
							if (data && data.error_description) {
								deferred.reject(data.error_description);
							} else {
								deferred.reject('Unable to contact server; please, try again later.');
							}
						});
					return deferred.promise;
				},
				forgotPassword : function (email) {
					var deferred = $q.defer();

					$http.get(apiUrlBase+'/user/forgotPassword/'+email)
						.success(function(data) {		
							deferred.resolve(data);
						})
						.error(function(data) {
							if (data && data.error_description) {
								deferred.reject(data.error_description);
							} else {
								deferred.reject('Unable to contact server; please, try again later.');
							}
						});
					return deferred.promise;

				}
			};

		}
	]);
/* messageService */

angular.module('webapp.directives')
    .service('service.crud', [ '$log', 'service.message', function($log, messageService) {


        var service = function(endpoint) {

            var crud = {
                display: false,
                list: {
                    items: [],
                    count: 0
                },
                currentItem: {}
            };
            
            
            crud.subscribe = function (scope) {
                endpoint.onUpdate(scope, function(callb) {
                    scope.$apply(function () {
                        endpoint.applyUpdateToList(callb, crud.list.items);
                    });
                });
            };

            endpoint.getAllPaged().then(function (data) {
                angular.extend(crud.list, data);
                
            }, $log.error);

            crud.showAdd = function() {
                crud.display = true;
                crud.currentItem = {};
            };

            crud.showEdit = function(item) {
                crud.display = true;
                crud.currentItem = item;
            };

            crud.removeItem = function(item) {

                console.log(item);
                endpoint.delete(item.id, crud.currentItem).then(function() {
                    var index = crud.list.items.indexOf(item);
                    if (index > -1) {
                        crud.list.items.splice(index, 1);
                    }
                }, function(message) {
                    messageService.error(message);
                });
            };

            crud.cancel = function() {
                crud.display = false;
            };

            crud.save = function() {
                crud.display = false;
                if (crud.currentItem.id) {

                    endpoint.put(crud.currentItem.id, crud.currentItem).then(function() {

                    }, function(message) {
                        crud.display = true;
                        messageService.error(message);
                    });
                } else {

                    endpoint.post(crud.currentItem).then(function (result) {
                        var found = false;
                        var list = crud.list.items;
                        angular.forEach(list, function (value) {
                            if (result.id == value.id) {
                                angular.copy(result, value);
                                found = true;
                            }
                        });
                        if (!found) {
                            list.push(result);
                        }

                    }, function(message) {
                        crud.display = true;
                        messageService.error(message);
                    });
                }
            };

            crud.delete = function() {
                crud.display = false;

            };
            return crud;
        };

        /*
         * Private methods
         */


        return service;

    }]);

/* service.endPoint */

angular.module('webapp.services')
	.service('service.endPoint', ['$log', 'service.signalr', 'service.authorization`', '$q', '$rootScope', 'apiUrlBase', '$http',
		function ($log, serviceSignalr, authorizationService, $q, $rootScope, apiUrlBase, $http) {

			/* 
             * Service
             */
		    var returnService = function (basePath) {

		        return {
		            getAll: function (filter) {
		                return httpCall('GET', pathCombine(apiUrlBase, basePath) + "?" + filter);
		            },
		            getAllPaged: function (filter) {
		                return httpCall('GET', pathCombine(apiUrlBase, basePath) + "?" + filter + '&$inlinecount=allpages');
		            },
		            getDetailAll: function (filter) {
		                return httpCall('GET', pathCombine(apiUrlBase, basePath, 'Detail') + "?" + filter);
		            },
		            getDetailAllPaged: function (filter) {
		                return httpCall('GET', pathCombine(apiUrlBase, basePath, 'Detail') + "?" + filter + '&$inlinecount=allpages');
		            },
		            get: function(id) {
		                return httpCall('GET', pathCombine(apiUrlBase, basePath,id));
		            },
		            post: function(user) {
		                return httpCall('POST', pathCombine(apiUrlBase, basePath),user);
		            },
		            put: function(id, user) {
		                return httpCall('PUT', pathCombine(apiUrlBase, basePath,id), user);
		            },
		            delete: function (id) {
		                return httpCall('DELETE', pathCombine(apiUrlBase, basePath, id));
		            },
		            applyUpdateToList: applyUpdateToList,
		            toCamel: serviceSignalr.toCamel,
		            onUpdate: function (scope, callBack) {
		                
		                var notificationHub = null;
		                serviceSignalr.whenConnected().then(function () {
		                    notificationHub = serviceSignalr.getNotificationHub();
		                    notificationHub.invoke('subscribeToUpdates', basePath);
		                });
		                var destroy = $rootScope.$on("NotificationHub.OnUpdate", function (onId, update) {
		                    if (update.type == basePath) {
		                        var camel = serviceSignalr.toCamel(update.value);
		                        defaultUpdate(scope, camel, callBack);
		                    }
		                });
		                scope.$on("$destroy", function () {
		                    notificationHub.invoke('UnsubscribeFromUpdates', basePath);
		                    destroy();
		                });
		                return destroy;
		            }
		        };
		    };

		    /* 
             * Private
             */
		    function pathCombine() {
		        var path = arguments[0];
		        for (var i = 1; i < arguments.length; i++) {
		            path += '/' + arguments[i];
		        }
		        return path;
		    }

		    function httpCall(method, url, data) {
		        var deferred = $q.defer();

		        var config = {
		            method: method,
		            url: url,
		            headers: {
		                'Authorization': 'bearer ' + authorizationService.currentSession().accessToken,
		            },
                    data : data
		        };

		        $http(config)
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        $log.error("Request ERROR: ", data);
                        if (data && data.message) {
                            deferred.reject(data.message);
                        } else {
                            deferred.reject('Unable to contact server; please, try again later.');
                        }
                    });
		        return deferred.promise;
		    }

			function defaultUpdate(scope, update, callBack) {
			    if (angular.isArray(callBack)) {
			        scope.$apply(function () {
			            applyUpdateToList(update, callBack);
			        });

			    } else {
			        callBack(update);
			    }
			}

			function applyUpdateToList(update, list) {
			    var found = false;
			    angular.forEach(list, function (value,key) {
			        if (update.value.id == value.id) {
			            if (update.updateType !== 2) {
			                angular.copy(update.value, value);
			            } else {
			                list.splice(key, 1);
			            }
			            found = true;
			        }
			    });
			    if (!found && update.updateType === 0) {
			        list.push(update.value);
			    }
		    }

		    return returnService;

		}
	]);
/* messageService */

angular.module('webapp.services')
    .service('service.message', ['$log',
        function($log) {

            /*
             * Private methods
             */
            var timeSpan = 4000;
            /* 
             * Service
             */
            return {
                info: function(message) {
                    $log.info(message);
                    Materialize.toast(message, timeSpan);
                },
                warn: function(message) {
                    $log.warn(message);
                    Materialize.toast(message, timeSpan);
                },
                error: function(message) {
                    $log.error(message);
                    Materialize.toast(message, timeSpan);
                },
                debug: function(message) {
                    $log.debug(message);
                    Materialize.toast(message, timeSpan);
                }

            };

        }
    ]);

/* service.project */

angular.module('webapp.services')
	.service('service.project', ['$log', 'service.endPoint',
		function ($log, serviceEndPoint) {

			/* 
             * Service
             */
		    var projectHub = {};
		    var returnService = serviceEndPoint('Project', projectHub);

		    return returnService;

		}
	]);
/* service.signalr */

angular.module('webapp.services')
    .service('service.signalr', [
        '$log', 'signalrBase', 'service.authorization`', '$q', '$rootScope',
        function($log, signalrBase, authorizationService, $q, $rootScope) {

            var currentConnectionString = null;
            var currentConnectionDefer = null;
            var connection = null;
            var notificationHub = {};


            /* 
             * Service
             */
            var returnService = {
                whenConnected: function() {
                    if (currentConnectionString != authorizationService.currentSession().accessToken) {
                        if (connection) {
                            connection.stop();
                            connection = null;
                        }
                        currentConnectionString = authorizationService.currentSession().accessToken;
                        currentConnectionDefer = createConnection();
                    }
                    return currentConnectionDefer;
                },
                getNotificationHub: function() {
                    return notificationHub;
                },
                toCamel: function(o) {
                    var build, key, destKey, value;

                    if (o instanceof Array) {
                        build = [];
                        for (key in o) {
                            value = o[key];

                            if (typeof value === "object") {
                                value = returnService.toCamel(value);
                            }
                            build.push(value);
                        }
                    } else {
                        build = {};
                        for (key in o) {
                            destKey = (key.charAt(0).toLowerCase() + key.slice(1) || key).toString();
                            value = o[key];
                            if (typeof value === "object") {
                                value = returnService.toCamel(value);
                            }
                            build[destKey] = value;
                        }
                    }
                    return build;
                }
            };

            /*
             * Private methods
             */
            function createConnection() {

                var connectionDefer = $q.defer();
                console.log("Authenticated:" + signalrBase);
                connection = $.hubConnection(signalrBase);
                connection.qs = { "bearer": authorizationService.currentSession().accessToken };

                /*
                 * Register events
                 */
                notificationHub = connection.createHubProxy('NotificationHub');
                notificationHub.on('OnUpdate', function (typeName, value) {
                    $rootScope.$emit("NotificationHub.OnUpdate", { type: typeName, value: value });
                });

                var start = connection.start();

              

                start.done(function() {
                    connectionDefer.resolve(connection);
                });
                start.fail(function(result) {
                    connectionDefer.reject(result.message);
                });
                return connectionDefer.promise;
            }

            return returnService;

        }
    ]);
/* service.user */

angular.module('webapp.services')
	.service('service.user', ['$log', 'service.endPoint',
		function ($log, serviceEndPoint) {

			/* 
             * Service
             */
		    var userHub = {};
		    var returnService = serviceEndPoint('User', userHub);

		    return returnService;

		}
	]);