using NewSample.Api.Common;
using NewSample.Core.BusinessLogic.Components.Interfaces;
using NewSample.Shared.Faction;
using NewSample.Shared.Faction.Reference;
using Moq;
using NewSample.Dal.Models;
using NUnit.Framework;

namespace NewSample.Api.Tests.Common
{
    [TestFixture]
    public class FactionCommonControllerTests : BaseCommonControllerTests<Faction, FactionModel, FactionReferenceModel, FactionCreateUpdateModel, IFactionManager>
    {
        private Mock<IFactionManager> _mockIFactionManager;
        private FactionCommonController _factionCommonController;


        #region Overrides of BaseCommonControllerTests

        public override void Setup()
        {
            _mockIFactionManager = new Mock<IFactionManager>(MockBehavior.Strict);
            _factionCommonController = new FactionCommonController(_mockIFactionManager.Object);
            base.Setup();
        }

        protected override Mock<IFactionManager> GetManager()
        {
            return _mockIFactionManager;
        }

        protected override BaseCommonController<Faction, FactionModel, FactionReferenceModel, FactionCreateUpdateModel> GetCommonController()
        {
            return _factionCommonController;
        }

        public override void TearDown()
        {
            base.TearDown();
            _mockIFactionManager.VerifyAll();
        }

        #endregion

    }
}