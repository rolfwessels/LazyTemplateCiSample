using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NewSample.Api.Tests")]
[assembly: NewSample.Utilities.Helpers.Log4NetInitialize("NewSample.Api.Tests")]
[assembly: log4net.Config.XmlConfigurator]
[assembly: AssemblyDescription("Contains all NewSample unit tests")]