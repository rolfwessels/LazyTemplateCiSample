using System.Reflection;

[assembly: AssemblyTitle("NewSample.Utilities.Tests")]
[assembly: AssemblyDescription("Contains all NewSample.Utilities unit tests.")]
[assembly: NewSample.Utilities.Helpers.Log4NetInitialize("NewSample.Utilities.Tests")]
[assembly: log4net.Config.XmlConfigurator]
