using System.Threading.Tasks;
using NewSample.Shared.Interfaces.Base;
using NewSample.Shared.Models;

namespace NewSample.Shared.Interfaces.Shared
{
    public interface IProjectControllerActions : ICrudController<ProjectModel, ProjectCreateUpdateModel>
	{
	}
}