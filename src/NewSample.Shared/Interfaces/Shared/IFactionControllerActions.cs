using System.Threading.Tasks;
using NewSample.Shared.Faction;
using NewSample.Shared.Interfaces.Base;

namespace NewSample.Shared.Interfaces.Shared
{
    public interface IFactionControllerActions : ICrudController<FactionModel, FactionCreateUpdateModel>
	{
	}
}