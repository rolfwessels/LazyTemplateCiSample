using System.Collections.Generic;
using System.Threading.Tasks;
using NewSample.Shared.Interfaces.Base;
using NewSample.Shared.Models;

namespace NewSample.Shared.Interfaces.Shared
{
    public interface IUserControllerActions : ICrudController<UserModel, UserCreateUpdateModel>
	{
	    Task<UserModel> Register(RegisterModel user);
	    Task<bool> ForgotPassword(string email);
        Task<UserModel> WhoAmI();
        Task<List<RoleModel>> Roles();
	}
}