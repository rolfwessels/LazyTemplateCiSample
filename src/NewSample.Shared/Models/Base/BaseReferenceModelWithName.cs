namespace NewSample.Shared.Models.Base
{
    public abstract class BaseReferenceModelWithName : BaseReferenceModel
    {
        public string Name { get; set; }
    }
}