using System;
using NewSample.Shared.Models.Interfaces;

namespace NewSample.Shared.Models.Base
{
    public abstract class BaseModel : IBaseModel
    {
        public string Id { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}