using System;

namespace NewSample.Shared.Models.Interfaces
{
    public interface IBaseModel
    {
        string Id { get; set; }
    }
}