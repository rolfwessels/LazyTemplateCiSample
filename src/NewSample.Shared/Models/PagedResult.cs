using System.Collections.Generic;

namespace NewSample.Shared.Models
{
    public class PagedResult<T>
    {
        public List<T> Items { get; set; }
        public int Count { get; set; }
    }
}