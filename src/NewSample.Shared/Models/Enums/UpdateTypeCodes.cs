﻿namespace NewSample.Shared.Models.Enums
{
    public enum UpdateTypeCodes
    {
        Inserted,
        Updated,
        Removed
    }
}