using NewSample.Shared.Models.Base;

namespace NewSample.Shared.Models
{
    public class ProjectModel : BaseModel
    {
        public string Name { get; set; }
    }
}