using System.Collections.Generic;

namespace NewSample.Shared.Models
{
    public class RoleModel 
    {
        public string Name { get; set; }
        public List<string> Activities { get; set; }
    }
}