using NewSample.Shared.Models.Base;

namespace NewSample.Shared.Faction
{
    public class FactionModel : BaseModel
    {
        public string Name { get; set; }

        public string Seat { get; set; }

        public int Bla { get; set; }
    }
}