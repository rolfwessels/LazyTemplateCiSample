using NewSample.Shared.Models.Base;

namespace NewSample.Shared.Models.Reference
{
    public class UserReferenceModel : BaseReferenceModelWithName
    {
        public string Email { get; set; }
    }
}