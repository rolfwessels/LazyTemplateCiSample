using Autofac;
using FluentValidation;
using Lazy.Authentication.OAuth2.Dal.Interfaces;
using NewSample.Core.BusinessLogic.Components;
using NewSample.Core.BusinessLogic.Components.Interfaces;
using NewSample.Core.MessageUtil;
using NewSample.Dal.Models;
using NewSample.Dal.Persistance;
using NewSample.Dal.Validation;
using IValidatorFactory = NewSample.Dal.Validation.IValidatorFactory;

namespace NewSample.Core.Startup
{
	public abstract class IocCoreBase
	{
		protected void SetupCore(ContainerBuilder builder)
		{
            SetupMongoDb(builder);
		    SetupManagers(builder);
			SetupTools(builder);
            SetupValidation(builder);
		}

	    protected virtual void SetupMongoDb(ContainerBuilder builder)
	    {
	        builder.Register(GetInstanceOfIGeneralUnitOfWorkFactory).SingleInstance();
	        builder.Register(x => x.Resolve<IGeneralUnitOfWorkFactory>().GetConnection());
	    }

	    private static void SetupManagers(ContainerBuilder builder)
		{
            builder.RegisterType<BaseManagerArguments>();
            builder.RegisterType<ApplicationManager>().As<IApplicationManager>();
            builder.RegisterType<OAuthDataManager>().As<IOAuthDataManager>();
            builder.RegisterType<ProjectManager>().As<IProjectManager>();
            builder.RegisterType<RoleManager>().As<IRoleManager>();
            builder.RegisterType<UserManager>().As<IUserManager>();
            builder.RegisterType<FactionManager>().As<IFactionManager>();
		}

	    private static void SetupValidation(ContainerBuilder builder)
	    {
            builder.RegisterType<ValidatorFactory>().As<IValidatorFactory>();
	        builder.RegisterType<UserValidator>().As<IValidator<User>>();
	        builder.RegisterType<ProjectValidator>().As<IValidator<Project>>();
	        builder.RegisterType<FactionValidator>().As<IValidator<Faction>>();
	        builder.RegisterType<UserValidator>().As<IValidator<User>>();
	    }

	    private void SetupTools(ContainerBuilder builder)
		{
			builder.Register((x) => Messenger.Default).As<IMessenger>();
		}

        protected abstract IGeneralUnitOfWorkFactory GetInstanceOfIGeneralUnitOfWorkFactory(IComponentContext arg);
	}
}
