﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NewSample.Dal.Models;

namespace NewSample.Core.BusinessLogic.Components.Interfaces
{
    public interface IRoleManager 
    {
        Task<Role> GetRoleByName(string name);
        Task<List<Role>> Get();
    }
}