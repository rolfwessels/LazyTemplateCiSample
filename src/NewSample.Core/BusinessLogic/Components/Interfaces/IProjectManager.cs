using NewSample.Dal.Models;

namespace NewSample.Core.BusinessLogic.Components.Interfaces
{
    public interface IProjectManager : IBaseManager<Project>
    {
    }
}