using NewSample.Dal.Models;

namespace NewSample.Core.BusinessLogic.Components.Interfaces
{
    public interface IApplicationManager : IBaseManager<Application>
    {
        Application GetApplicationById(string clientId);
    }
}