using NewSample.Core.BusinessLogic.Components.Interfaces;
using NewSample.Dal.Models;
using NewSample.Dal.Persistance;

namespace NewSample.Core.BusinessLogic.Components
{
    public class FactionManager : BaseManager<Faction>, IFactionManager
    {
        public FactionManager(BaseManagerArguments baseManagerArguments) : base(baseManagerArguments)
        {
        }

        #region Overrides of BaseManager<Faction>

        protected override IRepository<Faction> Repository
        {
            get { return _generalUnitOfWork.Factions; }
        }

        #endregion
    }
}