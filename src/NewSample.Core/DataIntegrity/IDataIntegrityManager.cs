﻿using System.Threading.Tasks;
using NewSample.Dal.Models;

namespace NewSample.Core.DataIntegrity
{
    public interface IDataIntegrityManager
    {
        Task<long> UpdateAllReferences<T>(T updatedValue);
        Task<long> GetReferenceCount<T>(T updatedValue);
    }
}