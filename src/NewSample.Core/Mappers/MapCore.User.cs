﻿using AutoMapper;
using NewSample.Dal.Models;
using NewSample.Dal.Models.Reference;

namespace NewSample.Core.Mappers
{
    public static partial class MapCore
	{
        public static void CreateUserMap()
        {
            Mapper.CreateMap<User, UserReference>();
        }

        public static UserReference ToReference(this User user, UserReference userReference = null)
        {
            return Mapper.Map(user, userReference);
        }
	}
}