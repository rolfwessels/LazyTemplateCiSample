using AutoMapper;
using NewSample.Dal.Models;

namespace NewSample.Core.Mappers
{
    public static partial class MapCore
	{
        public static void CreateFactionMap()
        {
            Mapper.CreateMap<Faction, FactionReference>();
        }

        public static FactionReference ToReference(this Faction faction, FactionReference factionReference = null)
        {
            return Mapper.Map(faction, factionReference);
        }
	}
}