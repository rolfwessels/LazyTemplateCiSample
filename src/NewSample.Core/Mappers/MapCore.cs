namespace NewSample.Core.Mappers
{
    public static partial class MapCore
    {
        static MapCore()
        {
            CreateProjectMap();
            CreateFactionMap();
            CreateOAuthMap();
            CreateUserMap();
        }

    }
}
