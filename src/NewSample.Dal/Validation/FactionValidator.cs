using FluentValidation;
using NewSample.Dal.Models;

namespace NewSample.Dal.Validation
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Interoperability", "CA1405:ComVisibleTypeBaseTypesShouldBeComVisible")]
    public class FactionValidator : AbstractValidator<Faction>
    {
        public FactionValidator()
        {
            RuleFor(x => x.Name).NotNull().MediumString();
        }
    }
}