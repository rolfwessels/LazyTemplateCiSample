﻿using FluentValidation;
using FluentValidation.Results;
using NewSample.Dal.Models;

namespace NewSample.Dal.Validation
{
    public interface IValidatorFactory
    {
        ValidationResult For<T>(T user);
        void ValidateAndThrow<T>(T user);
        IValidator<T> Validator<T>();
    }

    
}