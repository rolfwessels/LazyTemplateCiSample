using System;
using NewSample.Dal.Models;

namespace NewSample.Dal.Persistance
{
	public interface IGeneralUnitOfWork : IDisposable
	{
		IRepository<User> Users { get;  }
		IRepository<Application> Applications { get; }
		IRepository<Project> Projects { get; }
		IRepository<Faction> Factions { get; }
	}
}
