namespace NewSample.Dal.Models.Enums
{
	public enum Activity
	{
        Subscribe = 001,

		ReadUsers = 100 ,
		UpdateUsers = 101 ,
        InsertUsers = 102,
        DeleteUser = 103,

        ReadProject = 200,
        UpdateProject = 201,
        InsertProject = 202,
        DeleteProject = 203,
        
        ReadFaction = 300,
        UpdateFaction = 301,
        InsertFaction = 302,
        DeleteFaction = 303,
        SubscribeFaction = 304,
	}
}
