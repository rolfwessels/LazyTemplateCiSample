﻿namespace NewSample.Dal.Models.Enums
{
	public enum UpdateTypes
	{
		Inserted,
		Updated,
		Removed
	}
}