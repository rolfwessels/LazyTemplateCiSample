namespace NewSample.Dal.Models
{
	public class Faction : BaseDalModelWithId
	{
		public string Name { get; set; }

	    public string Seat { get; set; }

	    public int Bla { get; set; }

        public override string ToString()
	    {
	        return string.Format("Faction: {0}", Name);
	    }
	}
}