﻿using System;

namespace NewSample.Dal.Models.Base
{
    public interface IBaseReference
    {
        string Id { get; set; }
    }
}