﻿using System;
using NewSample.Dal.Models.Interfaces;

namespace NewSample.Dal.Models
{
	public abstract class BaseDalModelWithId : BaseDalModel, IBaseDalModelWithId
	{
		public string Id { get; set; }
	}

}