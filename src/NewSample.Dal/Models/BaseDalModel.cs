﻿using System;
using NewSample.Dal.Models.Interfaces;

namespace NewSample.Dal.Models
{
	public abstract class BaseDalModel : IBaseDalModel
	{
		public BaseDalModel()
		{
			CreateDate = DateTime.Now;
			UpdateDate = DateTime.Now;
		}

		public DateTime CreateDate { get; set; }
		public DateTime UpdateDate { get; set; }
	}
}