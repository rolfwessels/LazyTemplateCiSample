﻿using NewSample.Dal.Models.Base;

namespace NewSample.Dal.Models.Reference
{
    public class UserReference : BaseReferenceWithName
    {
        public string Email { get; set; }
    }
}