﻿using System.Collections.Generic;
using NewSample.Dal.Models.Enums;

namespace NewSample.Dal.Models
{
	public class Role
	{
		public string Name { get; set; }
		public List<Activity> Activities { get; set; }
	}
}