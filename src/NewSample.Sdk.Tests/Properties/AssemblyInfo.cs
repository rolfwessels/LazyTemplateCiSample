using System.Reflection;

[assembly: AssemblyTitle("NewSample.Sdk.Tests")]
[assembly: AssemblyDescription("Contains all NewSample.Sdk unit tests.")]
[assembly: NewSample.Utilities.Helpers.Log4NetInitialize("NewSample.Sdk.Tests")]
[assembly: log4net.Config.XmlConfigurator]
