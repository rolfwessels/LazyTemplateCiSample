using System.Collections.Generic;
using FizzWare.NBuilder;
using NewSample.Api.Tests.Helper;
using NewSample.Sdk.RestApi;
using NewSample.Sdk.Tests.Shared;
using NewSample.Shared.Faction;
using NewSample.Shared.Faction.Reference;
using NUnit.Framework;

namespace NewSample.Sdk.Tests.WebApi
{
	[TestFixture]
	[Category("Integration")]
    public class FactionApiClientTests : CrudComponentTestsBase<FactionModel, FactionCreateUpdateModel, FactionReferenceModel>
	{
		private FactionApiClient _factionApiClient;

	    #region Setup/Teardown

	    protected override void Setup()
		{
            _factionApiClient = new FactionApiClient(_adminRequestFactory.Value);
            SetRequiredData(_factionApiClient);
		}

	    [TearDown]
		public void TearDown()
		{

		}

	    protected override IList<FactionCreateUpdateModel> GetExampleData()
	    {
	        var factionDetailModel = Builder<FactionCreateUpdateModel>.CreateListOfSize(2).All().WithValidModelData().Build();
	        return factionDetailModel;
	    }

	    #endregion
	}

    
}