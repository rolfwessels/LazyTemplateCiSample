using System.Collections.Generic;
using FizzWare.NBuilder;
using NewSample.Api.Tests.Helper;
using NewSample.Sdk.RestApi;
using NewSample.Sdk.Tests.Shared;
using NewSample.Shared.Models;
using NewSample.Shared.Models.Reference;
using NUnit.Framework;

namespace NewSample.Sdk.Tests.WebApi
{
	[TestFixture]
	[Category("Integration")]
    public class ProjectApiClientTests : CrudComponentTestsBase<ProjectModel, ProjectCreateUpdateModel, ProjectReferenceModel>
	{
		private ProjectApiClient _projectApiClient;

	    #region Setup/Teardown

	    protected override void Setup()
		{
            _projectApiClient = new ProjectApiClient(_adminRequestFactory.Value);
            SetRequiredData(_projectApiClient);
		}

	    [TearDown]
		public void TearDown()
		{

		}

	    protected override IList<ProjectCreateUpdateModel> GetExampleData()
	    {
	        var projectDetailModel = Builder<ProjectCreateUpdateModel>.CreateListOfSize(2).All().WithValidModelData().Build();
	        return projectDetailModel;
	    }

	    #endregion
	}

    
}