﻿using System;
using System.IO;
using System.Reflection;
using NewSample.Api;
using NewSample.Api.AppStartup;
using NewSample.Sdk.OAuth;
using NewSample.Sdk.RestApi;
using NewSample.Shared.Models;
using Microsoft.Owin.Hosting;
using NCrunch.Framework;
using RestSharp;
using log4net;
using NewSample.Core.Tests.Helpers;

namespace NewSample.Sdk.Tests.Shared
{
    public class IntegrationTestsBase
    {
        public const string ClientId = "NewSample.Api";
        public const string AdminPassword = "admin!";
        public const string AdminUser = "admin";
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Lazy<string> _hostAddress;


        protected static Lazy<RestConnectionFactory> _defaultRequestFactory;
        protected static Lazy<RestConnectionFactory> _adminRequestFactory;
        private static TokenResponseModel _adminToken;

        static IntegrationTestsBase()
        {
            _hostAddress = new Lazy<string>(StartHosting);
            _defaultRequestFactory = new Lazy<RestConnectionFactory>(() => new RestConnectionFactory(_hostAddress.Value));
            _adminRequestFactory = new Lazy<RestConnectionFactory>(CreateAdminRequest);
        }



        public string SignalRUri
        {
            get { return _defaultRequestFactory.Value.GetClient().BuildUri(new RestRequest("signalr")).ToString(); }
        }

        public static TokenResponseModel AdminToken
        {
            get
            {
                if (_adminToken == null)
                {
                    _log.Info("Create admin request: " + _adminRequestFactory.Value);
                }

                return _adminToken;
            }

        }

        #region Private Methods

        private static string StartHosting()
        {
            int port = new Random().Next(9000, 9999);
            string address = string.Format("http://localhost:{0}/", port);
            _log.Info(string.Format("Starting api on [{0}]", address));


          var websitePath = Path.Combine(TestHelper.GetSourceBasePath(),@"NewSample.Website");

          SimpleFileServer.PossibleWebBasePath = new [] { websitePath};
            WebApp.Start<Startup>(address);
            return address;
        }


        private static RestConnectionFactory CreateAdminRequest()
        {
            var restConnectionFactory = new RestConnectionFactory(_hostAddress.Value);
            
            var oAuthConnection = new OAuthApiClient(restConnectionFactory);
            _adminToken = oAuthConnection.GenerateToken(new TokenRequestModel
                {
                    UserName = AdminUser, ClientId = ClientId, Password = AdminPassword
                }).Result;
            
            return restConnectionFactory;
        }

        #endregion
    }
}