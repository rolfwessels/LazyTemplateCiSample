using System;
using FizzWare.NBuilder;
using FluentAssertions;
using NewSample.Core.BusinessLogic.Components;
using NewSample.Core.BusinessLogic.Components.Interfaces;
using NewSample.Core.MessageUtil;
using NewSample.Core.Tests.Fakes;
using NewSample.Dal.Models;
using NewSample.Dal.Persistance;
using NewSample.Dal.Validation;
using NewSample.Utilities.Helpers;
using Moq;
using NUnit.Framework;

namespace NewSample.Core.Tests.Managers
{
    [TestFixture]
    public class BaseManagerTests
    {
        protected BaseManagerArguments _baseManagerArguments;
        protected FakeGeneralUnitOfWork _fakeGeneralUnitOfWork;
        protected Mock<IMessenger> _mockIMessenger;
        protected Mock<IValidatorFactory> _mockIValidatorFactory;

        #region Setup/Teardown

        public virtual void Setup()
        {
            _mockIMessenger = new Mock<IMessenger>();
            _mockIValidatorFactory = new Mock<IValidatorFactory>();
            _fakeGeneralUnitOfWork = new FakeGeneralUnitOfWork();
            _baseManagerArguments = new BaseManagerArguments(_fakeGeneralUnitOfWork, _mockIMessenger.Object,
                                                             _mockIValidatorFactory.Object);
        }

        [TearDown]
        public virtual void TearDown()
        {
            _mockIValidatorFactory.VerifyAll();
            _mockIMessenger.VerifyAll();
        }

        #endregion
    }

}