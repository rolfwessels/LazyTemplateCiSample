using FizzWare.NBuilder;
using NewSample.Core.BusinessLogic.Components;
using NewSample.Dal.Models;
using NewSample.Dal.Persistance;
using NUnit.Framework;

namespace NewSample.Core.Tests.Managers
{
    [TestFixture]
    public class ProjectManagerTests : BaseTypedManagerTests<Project>
    {
        private ProjectManager _projectManager;

        #region Setup/Teardown

        public override void Setup()
        {
            base.Setup();
            _projectManager = new ProjectManager(_baseManagerArguments);
        }

        #endregion

        protected override IRepository<Project> Repository
        {
            get { return _fakeGeneralUnitOfWork.Projects; }
        }

        protected override Project SampleObject
        {
            get { return Builder<Project>.CreateNew().Build(); }
        }

        protected override BaseManager<Project> Manager
        {
            get { return _projectManager; }
        }
    }
}