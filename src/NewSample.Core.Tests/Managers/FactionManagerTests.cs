using FizzWare.NBuilder;
using NewSample.Core.BusinessLogic.Components;
using NewSample.Dal.Models;
using NewSample.Dal.Persistance;
using NUnit.Framework;

namespace NewSample.Core.Tests.Managers
{
    [TestFixture]
    public class FactionManagerTests : BaseTypedManagerTests<Faction>
    {
        private FactionManager _factionManager;

        #region Setup/Teardown

        public override void Setup()
        {
            base.Setup();
            _factionManager = new FactionManager(_baseManagerArguments);
        }

        #endregion


        protected override IRepository<Faction> Repository
        {
            get { return _fakeGeneralUnitOfWork.Factions; }
        }

        protected override Faction SampleObject
        {
            get { return Builder<Faction>.CreateNew().Build(); }
        }

        protected override BaseManager<Faction> Manager
        {
            get { return _factionManager; }
        }
    }
}