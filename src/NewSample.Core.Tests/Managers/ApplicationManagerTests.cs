using FizzWare.NBuilder;
using NewSample.Core.BusinessLogic.Components;
using NewSample.Dal.Models;
using NewSample.Dal.Persistance;
using NUnit.Framework;

namespace NewSample.Core.Tests.Managers
{
    [TestFixture]
    public class ApplicationManagerTests : BaseTypedManagerTests<Application>
    {
        private ApplicationManager _userManager;

        #region Setup/Teardown

        public override void Setup()
        {
            base.Setup();
            _userManager = new ApplicationManager(_baseManagerArguments);
        }

        #endregion

        protected override IRepository<Application> Repository
        {
            get { return _fakeGeneralUnitOfWork.Applications; }
        }

        protected override Application SampleObject
        {
            get { return Builder<Application>.CreateNew().Build(); }
        }

        protected override BaseManager<Application> Manager
        {
            get { return _userManager; }
        }
    }
}