using System.Reflection;

[assembly: AssemblyTitle("NewSample.Core.Tests")]
[assembly: AssemblyDescription("Contains all NewSample Core unit tests.")]
[assembly: NewSample.Utilities.Helpers.Log4NetInitialize("NewSample.Core.Tests")]
[assembly: log4net.Config.XmlConfigurator]