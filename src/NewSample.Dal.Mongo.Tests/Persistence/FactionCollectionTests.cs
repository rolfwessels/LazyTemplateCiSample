using FizzWare.NBuilder;
using FizzWare.NBuilder.Generators;
using NewSample.Core.Tests.Helpers;
using NewSample.Dal.Models;
using NUnit.Framework;

namespace NewSample.Dal.Mongo.Tests.Persistence
{
    [TestFixture]
    public class FactionCollectionTests
    {
	
        #region Setup/Teardown

        public void Setup()
        {
			
        }

        [TearDown]
        public void TearDown()
        {
			
        }

        #endregion

        [Test]
        public void Users_GivenCrudCommands_ShouldAddListAndDeleteRecords()
        {
            // arrange
            Setup();;
            using (var dataContext = MongoConnectionFactory.New)
            {
                var faction = Builder<Faction>.CreateNew().WithValidData().Build();
                var persistanceTester = new PersistanceTester<Faction>(dataContext, work => work.Factions);
                persistanceTester.ValueValidate(x => x.Name, faction.Name, GetRandom.String(30));
                persistanceTester.ValidateCrud(faction).Wait();
            }
        }


    }
}