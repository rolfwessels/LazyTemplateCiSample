using FizzWare.NBuilder;
using FizzWare.NBuilder.Generators;
using NewSample.Core.Tests.Helpers;
using NewSample.Dal.Models;
using NUnit.Framework;

namespace NewSample.Dal.Mongo.Tests.Persistence
{
    [TestFixture]
    public class ProjectCollectionTests
    {
	
        #region Setup/Teardown

        public void Setup()
        {
			
        }

        [TearDown]
        public void TearDown()
        {
			
        }

        #endregion

        [Test]
        public void Users_GivenCrudCommands_ShouldAddListAndDeleteRecords()
        {
            // arrange
            Setup();;
            using (var dataContext = MongoConnectionFactory.New)
            {
                var project = Builder<Project>.CreateNew().WithValidData().Build();
                var persistanceTester = new PersistanceTester<Project>(dataContext, work => work.Projects);
                persistanceTester.ValueValidate(x => x.Name, project.Name, GetRandom.String(30));
                persistanceTester.ValidateCrud(project).Wait();
            }
        }


    }
}