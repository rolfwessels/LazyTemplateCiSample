﻿using System.Threading.Tasks;
using MongoDB.Driver;

namespace NewSample.Dal.Mongo.Migrations
{
    public interface IMigration
    {
        Task Update(IMongoDatabase db);
    }
}