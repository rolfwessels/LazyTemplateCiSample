﻿using System;
using System.Linq;
using System.Reflection;
using log4net;
using NewSample.Dal.Mongo.Properties;
using NewSample.Dal.Persistance;
using MongoDB.Driver;

namespace NewSample.Dal.Mongo
{
    public class MongoConnectionFactory : IGeneralUnitOfWorkFactory
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly string _connectionString;
        private readonly string _databaseName;
        private readonly Lazy<IGeneralUnitOfWork> _singleConnection;

        public MongoConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
            _databaseName = new Uri(_connectionString).Segments.Skip(1).FirstOrDefault() ?? "NewSample";
            _singleConnection = new Lazy<IGeneralUnitOfWork>(GeneralUnitOfWork);
        }

        public static IGeneralUnitOfWork New
        {
            get { return new MongoConnectionFactory(Settings.Default.Connection).GetConnection(); }
        }

        public string DatabaseName
        {
            get { return _databaseName; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
        }

        #region IGeneralUnitOfWorkFactory Members

        public IGeneralUnitOfWork GetConnection()
        {
            return _singleConnection.Value;
        }

        private IGeneralUnitOfWork GeneralUnitOfWork()
        {
            _log.Info("Create new connection to " + _connectionString);
            IMongoDatabase database = DatabaseOnly();
            _log.Info("Apply update");
            Configuration.Instance().Update(database).Wait();
            return new MongoGeneralUnitOfWork(database);
        }

        #endregion

        public IMongoDatabase DatabaseOnly()
        {
            IMongoClient client = ClientOnly();
            IMongoDatabase database = client.GetDatabase(_databaseName);
            return database;
        }

        #region Private Methods

        private IMongoClient ClientOnly()
        {
            return new MongoClient(_connectionString);
        }

        #endregion
    }
}