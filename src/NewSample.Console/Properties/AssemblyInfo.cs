using System.Reflection;

[assembly: AssemblyTitle("NewSample.Console")]
[assembly: AssemblyDescription("NewSample console application")]
[assembly: NewSample.Utilities.Helpers.Log4NetInitialize("NewSample.console")]
[assembly: log4net.Config.XmlConfigurator]