using System.Reflection;

[assembly: AssemblyTitle("NewSample.Shared.Tests")]
[assembly: AssemblyDescription("Contains all NewSample unit tests for the shared components")]
[assembly: NewSample.Utilities.Helpers.Log4NetInitialize("NewSample.Shared.Tests")]
[assembly: log4net.Config.XmlConfigurator]