using System.Collections.Generic;

namespace NewSample.Api.WebApi.ODataSupport
{
    public class ODataPageResult<T>
    {
        public List<T> Items { get; set; }

        public long? Count { get; set; }
    }
}