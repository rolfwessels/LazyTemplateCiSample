using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using NewSample.Api.Common;
using NewSample.Api.WebApi.Attributes;
using NewSample.Api.WebApi.ODataSupport;
using NewSample.Dal.Models.Enums;
using NewSample.Shared;
using NewSample.Shared.Interfaces.Base;
using NewSample.Shared.Interfaces.Shared;
using NewSample.Shared.Faction;
using NewSample.Shared.Faction.Reference;

namespace NewSample.Api.WebApi.Controllers
{

    /// <summary>
	///     Api controller for managing all the faction
	/// </summary>
    [RoutePrefix(RouteHelper.FactionController)]
    public class FactionController : ApiController, IFactionControllerActions, IBaseControllerLookups<FactionModel, FactionReferenceModel>
    {
	    private readonly FactionCommonController _factionCommonController;
	    
        public FactionController(FactionCommonController factionCommonController)
        {
            _factionCommonController = factionCommonController;
        }

        /// <summary>
        ///     Returns list of all the factions as references
        /// </summary>
        /// <returns>
        /// </returns>
        [Route,AuthorizeActivity(Activity.ReadFaction) , QueryToODataFilter]
        public Task<IEnumerable<FactionReferenceModel>> Get()
        {   
            return _factionCommonController.Get(Request.GetQuery());
        }

        /// <summary>
        /// GetCounter all factions with their detail.
        /// </summary>
        /// <returns></returns>
        [Route(RouteHelper.WithDetail),AuthorizeActivity(Activity.ReadFaction), QueryToODataFilter]
        public Task<IEnumerable<FactionModel>> GetDetail()
		{
		    return _factionCommonController.GetDetail(Request.GetQuery());
		}


        /// <summary>
		///     Returns a faction by his Id.
		/// </summary>
		/// <returns>
		/// </returns>
		[Route(RouteHelper.WithId),AuthorizeActivity(Activity.ReadFaction)]
		public Task<FactionModel> GetById(string id)
		{
            return _factionCommonController.GetById(id);
		}

	    /// <summary>
	    ///     Updates an instance of the faction item.
	    /// </summary>
	    /// <param name="id">The identifier.</param>
	    /// <param name="model">The faction.</param>
	    /// <returns>
	    /// </returns>
		[Route(RouteHelper.WithId),AuthorizeActivity(Activity.UpdateFaction) , HttpPut]
        public Task<FactionModel> Update(string id, FactionCreateUpdateModel model)
		{
            return _factionCommonController.Update(id, model);
		}

	    /// <summary>
	    ///     Add a new faction
	    /// </summary>
	    /// <param name="model">The faction.</param>
	    /// <returns>
	    /// </returns>
        [Route, AuthorizeActivity(Activity.InsertFaction), HttpPost]
		public Task<FactionModel> Insert(FactionCreateUpdateModel model)
		{
            return _factionCommonController.Insert(model);
		}

	    /// <summary>
	    ///     Deletes the specified faction.
	    /// </summary>
	    /// <param name="id">The identifier.</param>
	    /// <returns>
	    /// </returns>
		[Route(RouteHelper.WithId),AuthorizeActivity(Activity.DeleteFaction)]
        public Task<bool> Delete(string id)
		{
            return _factionCommonController.Delete(id);
		}

		
	}
}