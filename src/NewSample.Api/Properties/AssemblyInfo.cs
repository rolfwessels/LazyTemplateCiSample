using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Owin;

[assembly: AssemblyTitle("NewSample.Web")]
[assembly: AssemblyDescription("Contains all NewSample unit tests")]

[assembly: OwinStartup(typeof(NewSample.Api.Startup))]

[assembly: Guid("978213a9-1c25-43b2-ae40-32325c983c44")]
[assembly: NewSample.Utilities.Helpers.Log4NetInitialize("NewSample.Api")]
[assembly: log4net.Config.XmlConfigurator]

