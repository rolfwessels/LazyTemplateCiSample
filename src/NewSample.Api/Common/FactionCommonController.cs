using NewSample.Core.BusinessLogic.Components.Interfaces;
using NewSample.Dal.Models;
using NewSample.Shared.Interfaces.Shared;
using NewSample.Shared.Faction;
using NewSample.Shared.Faction.Reference;

namespace NewSample.Api.Common
{
    public class FactionCommonController : BaseCommonController<Faction, FactionModel, FactionReferenceModel, FactionCreateUpdateModel>, IFactionControllerActions
    {
        
        public FactionCommonController(IFactionManager factionManager)
            : base(factionManager)
        {
        }

    }
}