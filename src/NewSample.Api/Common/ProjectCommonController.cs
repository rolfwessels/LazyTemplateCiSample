using System.Linq;
using NewSample.Api.Models.Mappers;
using NewSample.Core.BusinessLogic.Components.Interfaces;
using NewSample.Dal.Models;
using NewSample.Shared.Interfaces.Shared;
using NewSample.Shared.Models;
using NewSample.Shared.Models.Reference;

namespace NewSample.Api.Common
{
    public class ProjectCommonController : BaseCommonController<Project, ProjectModel, ProjectReferenceModel, ProjectCreateUpdateModel>, IProjectControllerActions
    {
        
        public ProjectCommonController(IProjectManager projectManager)
            : base(projectManager)
        {
        }

    }
}
/* scaffolding [
    {
      "FileName": "IocApi.cs",
      "Indexline": "RegisterType<ProjectCommonController>",
      "InsertAbove": false,
      "InsertInline": false,
      "Lines": [
        "builder.RegisterType<ProjectCommonController>();"
      ]
    }
] scaffolding */