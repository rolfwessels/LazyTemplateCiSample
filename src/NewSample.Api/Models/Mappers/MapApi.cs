using AutoMapper;
using NewSample.Core.MessageUtil.Models;
using NewSample.Shared.Models;
using NewSample.Shared.Models.Enums;

namespace NewSample.Api.Models.Mappers
{
	public static partial class MapApi
	{
        static MapApi()
        {
            MapUserModel();
            MapProjectModel();
            MapFactionModel();
        }

		
		public static ValueUpdateModel<TModel> ToValueUpdateModel<T, TModel>(this DalUpdateMessage<T> updateMessage)
		{
			return new ValueUpdateModel<TModel>(Mapper.Map<T, TModel>(updateMessage.Value), (UpdateTypeCodes) updateMessage.UpdateType);
		}
	}
}
