using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using NewSample.Dal.Models;
using NewSample.Shared.Faction;
using NewSample.Shared.Faction.Reference;

namespace NewSample.Api.Models.Mappers
{
	public static partial class MapApi
	{
		private static void  MapFactionModel()
		{
			Mapper.CreateMap<Dal.Models.Faction, FactionModel>();
            Mapper.CreateMap<Dal.Models.Faction, FactionReferenceModel>();
			Mapper.CreateMap<FactionReference, FactionReferenceModel>();
			Mapper.CreateMap<FactionCreateUpdateModel, Dal.Models.Faction>().MapToDal();

		}

		public static Dal.Models.Faction ToDal(this FactionCreateUpdateModel model, Dal.Models.Faction faction = null)
		{
			return Mapper.Map(model, faction);
		}

		public static FactionModel ToModel(this Dal.Models.Faction faction, FactionModel model = null)
		{
			return Mapper.Map(faction, model);
		}

	    public static IEnumerable<FactionReferenceModel> ToReferenceModelList(IQueryable<Dal.Models.Faction> factions)
	    {
            return Mapper.Map<IQueryable<Dal.Models.Faction>, IEnumerable<FactionReferenceModel>>(factions);
	    }

        public static IEnumerable<FactionModel> ToModelList(IQueryable<Dal.Models.Faction> factions)
	    {
            return Mapper.Map<IQueryable<Dal.Models.Faction>, IEnumerable<FactionModel>>(factions);
	    }
	}
}