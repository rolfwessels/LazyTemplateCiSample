using System.Web.Http.Dependencies;
using NewSample.Api.AppStartup;
using NewSample.Api.Models.Mappers;
using NewSample.Api.SignalR;
using NewSample.Api.Swagger;
using NewSample.Api.WebApi;
using Owin;
using log4net.Config;

namespace NewSample.Api
{
	public class Startup
	{
	    
	    public void Configuration(IAppBuilder appBuilder)
		{
		    XmlConfigurator.Configure();
	        CrossOrginSetup.UseCors(appBuilder);
			BootStrap.Initialize(appBuilder);
		    MapApi.Initialize();
			WebApiSetup webApiSetup = WebApiSetup.Initialize(appBuilder, IocApi.Instance.Resolve<IDependencyResolver>());
			SignalRSetup.Initialize(appBuilder, IocApi.Instance.Resolve<Microsoft.AspNet.SignalR.IDependencyResolver>());
			SwaggerSetup.Initialize(webApiSetup.Configuration);
            SimpleFileServer.Initialize(appBuilder);
            webApiSetup.Configuration.EnsureInitialized();
		}
	}
}