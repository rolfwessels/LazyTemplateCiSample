/* messageService */

angular.module('newsample')
    .service('messages',
        function($log,toaster)    {

            /*
             * Private methods
             */
            var timeSpan = 4000;
            /*
             * Service
             */
            return {
                info: function(message) {
                    $log.info(message);
                    toaster.pop('success', "data-chain", message);
                },
                warn: function(message) {
                    $log.warn(message);
                    toaster.pop('warning', "data-chain", message);
                },
                error: function(message) {
                    $log.error(message);
                    toaster.pop('error', "data-chain", message);
                },
                debug: function(message) {
                    $log.debug(message);
                    toaster.pop('info', "data-chain", message);
                }

            };

        }
  );
