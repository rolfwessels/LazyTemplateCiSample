/* crudService */

angular.module('newsample')
  .service('crudService', function($log, messages, $stateParams) {

    var service = function(endpoint) {

      var crud = {
        display: false,
        list: {
          items: [],
          count: 0
        },
        currentItem: {}
      };


      crud.subscribe = function(scope) {
        endpoint.onUpdate(scope, function(callb) {
          scope.$apply(function() {
            endpoint.applyUpdateToList(callb, crud.list.items);
          });
        });
      };

      crud.start = function(scope) {
        endpoint.getDetailAllPaged("").then(function(data) {
          angular.extend(crud.list, data);
          if ($stateParams.id) {
            console.log("data: ", data.items);
            for (var i = 0; i < data.items.length; i++) {

              var item = data.items[i];
              console.log("$stateParams.id: ", item, $stateParams.id);
              if (item.id == $stateParams.id) {
                crud.showEdit(item);
              }
            }
          }
        }, $log.error);
      };

      crud.showAdd = function() {
        crud.display = true;
        crud.currentItem = {};
      };

      crud.showEdit = function(item) {
        crud.display = true;
        crud.currentItem = angular.copy(item);
      };

      crud.removeItem = function(item) {
        endpoint.delete(item.id, crud.currentItem).then(function() {
          crud.display = false;
          for (var i = 0; i < crud.list.items.length; i++) {
            var itm = crud.list.items[i];
            if (item.id == itm.id) {
              crud.list.items.splice(i, 1);
              break;
            }
          }
        }, function(message) {
          messages.error(message);
        });
      };



      crud.cancel = function() {
        crud.display = false;
      };

      crud.save = function() {
        crud.display = false;
        if (crud.currentItem.id) {

          endpoint.put(crud.currentItem.id, crud.currentItem).then(function(result) {
            crud.findOrUpdate(result);
          }, function(message) {
            crud.display = true;
            messages.error(message);
          });
        } else {

          endpoint.post(crud.currentItem).then(function(result) {
            crud.findOrUpdate(result);

          }, function(message) {
            crud.display = true;
            messages.error(message);
          });
        }
      };

      crud.findOrUpdate = function (item) {
        var found = false;
        var list = crud.list.items;
        angular.forEach(list, function(value) {
          if (item.id == value.id) {
            angular.copy(item, value);
            found = true;
          }
        });
        if (!found) {
          list.push(item);
        }
      };

      crud.delete = function() {
        crud.display = false;

      };
      return crud;
    };

    /*
     * Private methods
     */


    return service;

  });
