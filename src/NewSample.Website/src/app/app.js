'use strict';

angular.module('newsample', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngResource',
      'ui.router',
      'ui.bootstrap',
      'angular-flot',
      'ngTextTruncate',
      'angular-humanize',
      'localytics.directives',
      'LocalStorageModule',
      'toaster',
      'ui.gravatar',
      'angular-humanize'])
  .value('apiBase', '')
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('ui', {
            abstract: true,
            url: '/ui',
            templateUrl: 'components/common/content.html'
        })
        .state('ui.dashboard', {
          url: '/dashboard',
          templateUrl: 'app/dashboard/dashboard.html',
          data: { pageTitle: 'Dashboard' }
        })
        .state('ui.user', {
          url: '/user/:userId',
          templateUrl: 'app/user/userDashboard.html',
          data: { pageTitle: 'User' }
        })
        .state('ui.certificate', {
          url: '/certificate/:certificateId',
          templateUrl: 'app/certificate/certificateDashboard.html',
          data: { pageTitle: 'Certificate' }
        })
        .state('ui.faction', {
            url: '/faction/:factionId',
            templateUrl: 'app/faction/factionDashboard.html',
            data: { pageTitle: 'Faction' }
        })
        .state('ui.project', {
          url: '/project/:projectId',
          templateUrl: 'app/project/projectDashboard.html',
          data: { pageTitle: 'Project' }
        })
        .state('login', {
            url: '/login',
            templateUrl: 'app/login/login.html',
            data:{ pageTitle: 'Login', isSinglePage : true}
        })
        ;
    $urlRouterProvider.otherwise('/ui/dashboard');
  }).run( function(authorizationService) {
    authorizationService.isAuthenticatedOrRedirect();
  });
