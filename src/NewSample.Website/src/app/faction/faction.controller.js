'use strict';

angular.module('newsample')
  .controller('factionCtrl', function (apiFaction,crudService ) {
    var self = this;
    var scope = angular.extend(this,{
      crud : crudService(apiFaction)
    });
  });