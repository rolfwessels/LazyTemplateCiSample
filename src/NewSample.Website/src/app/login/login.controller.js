'use strict';

angular.module('newsample')
  .controller('loginCtrl', function ($log, messages, authorizationService,$location) {
    var self = this;
    var currentUser = authorizationService.currentSession();
    var scope = angular.extend(this,{
      email: currentUser.email||'admin',
      password: '',
      login : login,
      logout : logout
    });

    /*
     * Functions
     */
    function login() {

        var authenticate = authorizationService.authenticate(scope.email, scope.password);
        authenticate.then(function() {
            authorizationService.continueToPage();
        }, function(message) {
           scope.password = "";
            messages.error(message||'Invalid username or password.');
        },messages.info);

    }

    function logout() {
      messages.info("logout");
      authorizationService.logout();
    }




  });
