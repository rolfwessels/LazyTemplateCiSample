/* apiGraph */

angular.module('newsample')
  .service('polling', function($timeout) {



    var polling = function (apiCall,callBack,timeDelay) {
        var _apiCall = apiCall;
        var _timeDelay = timeDelay||callBack||5000;
        var _nextCall = null;
        var self = {
          reschedule : function () {
              self.stop();
              _nextCall = $timeout(self.runNow, _timeDelay);
          },
          runNow : function () {
              var response = _apiCall();
              if (response && response.then) {
              response.then(function (data) {
                  callBack(data);
                  self.reschedule();
              },function () {
                  self.reschedule();
              });
            }
            else {
              self.reschedule();
            }
          },
          start: function (scope) {
              self.runNow();
              scope.$on("$destroy", function() {
                  self.stop();
              });
          },
          stop : function () {
              if (_nextCall) $timeout.cancel(_nextCall);
              _nextCall = null;
          },
        }
        return self;

    };
    return polling;

  });
