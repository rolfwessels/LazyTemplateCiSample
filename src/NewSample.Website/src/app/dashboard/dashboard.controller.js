'use strict';

angular.module('newsample')
  .controller('dashboardCtrl', function (apiUser,apiProject,apiFaction,messages,$scope) {

    var scope = angular.extend(this,{
        allCounter : []
    });


    mapData(apiUser, 'Users', '#/user');
    mapData(apiProject, 'Projects', '#/project');
    mapData(apiFaction, 'Factions', '#/faction');

    function mapData(api,onScope,link) {
        var counter = { name: onScope, items: [], count: 0 , link : link};
        scope.allCounter.push(counter);
        api.onUpdate($scope, function (call) {
            $scope.$apply(function() {
                if (call.updateType === 0) {
                    counter.count += 1;
                } else if (call.updateType === 2) {
                    counter.count += -1;
                }
                update(api, counter);
            });
        });
        update(api, counter);
    }

    function update(api, counter) {
        api.getDetailAllPaged('$top=5&$orderby=updateDate desc').then(function (data) {
            counter.count = data.count;
            counter.items = data.items;
        }, messages.error, messages.debug);
    }
  });
