'use strict';


angular.module('newsample')
  .directive('search', function (apiEventLog,apiGraph,$timeout,polling) {
    return {
      restrict: 'E',
      scope: {
        options : "="
      },
      templateUrl: 'app/search/search.directive.html',
      link: function (scope, element, attrs) {
        var timeout = null;
        scope = angular.extend(scope, {
          eventLog: {}
        });

        scope.$watchGroup(['options.search','options.level'],function() {
          if (timeout) {
            $timeout.cancel(timeout);
          }
          timeout = $timeout(function() {
            updateResults();
            timeout = null;
          },500);

        });
        var refreshQueue = new polling(function () {
          updateResults();
        },60000);
        refreshQueue.start(scope);

        function updateResults() {
          apiEventLog.getAll(scope.options)
            .then(function (result) {
              scope.eventLog.list = result;
              console.log(scope.eventLog);

            });
        }
      }
    }
  });
