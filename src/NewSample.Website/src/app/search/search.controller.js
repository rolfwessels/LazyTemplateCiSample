'use strict';

angular.module('newsample')
  .controller('searchCtrl', function (apiEventLog,apiGraph,$scope,$timeout,polling) {
    var self = this;
    var timeout = null;


    self.options = {
        search: '',
        level: '',
        textLength: 100
    };

    self.overviewGraph = {};
    self.flotData = [];
    self.flotOptions = {

      series: {
        lines: {
          show: false,
          fill: true
        },

        splines: {
          show: true,
          tension: 0.4,
          lineWidth: 1,
          fill: 0.4
        },
        points: {
          radius: 0,
          show: true
        },
        shadowSize: 2,
        grow: {stepMode:'linear',stepDirection:'up',steps:80}
      },

      grid: {
        show:true,
        borderWidth: 0,
        autoHighlight : true
      },
      colors: ['#1ab394', '#1C84C6', '#f8ac59','#ed5565'],
      xaxis: {

        mode: 'time',
        tickSize: [3, 'day']
      },
      yaxis: {

      },
      tooltip: true
    };



    $scope.$watchGroup(['search.options.search','search.options.level'],function() {
      if (timeout) {
        $timeout.cancel(timeout);
      }
      timeout = $timeout(function() {
        updateResults();
        timeout = null;
      },500);

    });

    var refreshQueue = new polling(function () {
      updateResults();
    },60000);
    refreshQueue.start($scope);

    /**
     * Private
     */
    function updateResults() {

      apiGraph.getAll(self.options)
        .then(function (result) {
          self.flotData = graphToFlotTime(result,self.flotOptions);
          mapColors(result,self.flotOptions);
      });

    }


    function mapColors(model,options) {

      var options = {
        'DEBUG': '#1ab394',
        'INFO': '#1C84C6',
        'WARN': '#f8ac59',
        'ERROR': '#ed5565'
      };
      var colors = [];
      angular.forEach(model.series, function (labeln){
        colors.push(options[labeln]);
      });
      options.colors = colors;
    }

    function graphToFlotTime(model) {


      var data = [];
      angular.copy([],data);
      angular.forEach(model.data, function (row){
        var data1 = [];
        angular.forEach(row, function (item,ke1y){

          data1.push([new Date(model.labels[ke1y]).getTime(),item]);
        });

        data.push({grow:{stepMode:'linear'},data:data1 });
      });
      return data;

    }

    function graphToFlot(model,options) {
      options.xaxis.ticks = [];
      angular.forEach(model.labels, function (labeln,key){
        options.xaxis.ticks.push([key,labeln]);
      });
      var data = [];
      angular.copy([],data);
      angular.forEach(model.data, function (row){
        var data1 = [];
        angular.forEach(row, function (item,ke1y){
          data1.push([ke1y,item]);
        });

        data.push({data:data1 });
      });
      return data;

    }

  });
