'use strict';


angular.module('newsample')
    .filter('graphBest', function() {
        return function(input) {
        	var max = 0;
        	var maxIndex = 0;
		      angular.forEach(input.data[0],function (value,key) {
		      	if (max < value) {
		      		max = value;
		      		maxIndex = key;
		      	}
		      });
		      
          return  input.labels[maxIndex];
        };
    });
