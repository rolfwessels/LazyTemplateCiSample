'use strict';


angular.module('newsample')
    .directive('sparkline', function () {
        return {
          restrict: 'A',
          scope: {
            sparkData: '=',
            sparkOptions: '=',
            sparkGraph: '='
          },
          link: function (scope, element, attrs) {
            scope.$watch(scope.sparkData, function () {
              render();
            });
            scope.$watch(scope.sparkOptions, function(){
              render();
            });
            scope.$watch(scope.sparkGraph, function(){
              render();
            });

            var render = function () {

              var data = scope.sparkData;
              var options = scope.sparkOptions;
              if (scope.sparkGraph) {
                data = scope.sparkGraph.data[0];
               
                options = angular.extend({
                  tooltipFormat: '{{y}} ( {{offset:label}} )',
                  tooltipValueLookups: {
                      'label': scope.sparkGraph.labels
                    }
                },scope.sparkOptions);
                if (options.type === 'bar') {
                  options.tooltipFormat= '{{value}} ( {{offset:label}} )';
                  if (options.width === '100%') {
                    options.barSpacing = ($(element).width() / data.length) * 0.2;
                    options.barWidth = ($(element).width() - (data.length * options.barSpacing )) / data.length;
                  }
                }

              }
              

              $(element).sparkline(data, options);
            };
          }
        };
      });



