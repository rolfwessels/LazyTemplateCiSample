'use strict';


angular.module('newsample')
    .directive('time', function ($filter) {
        return {
          restrict: 'A',
          scope: {
            time: '=',
            fromTime: '=',
          },
          template: '<span ng-show="time" tooltip-placement="bottom" tooltip="{{long}}"><i class="fa fa-clock-o"></i> {{short}} </span>',
          link: function (scope, element, attrs) {
            var timeOut = (60*60*24*1); // 7 days
            scope.$watch('time',function () {
                scope.long = $filter('date')(scope.time,'dd-MMM-yyyy HH:mm:ss');
                scope.ssdate = $filter('asDate')(scope.fromTime || new Date());
                scope.sdate = $filter('asDate')(scope.time);
                if (Math.abs(scope.sdate - scope.ssdate) > timeOut) {
                    scope.short = $filter('date')(scope.time,'dd-MMM-yyyy');
                }
                else {
                  scope.short = $filter('humanizeRelativeTime')(scope.sdate);
                }
                if (scope.fromTime) {
                  scope.short =  $filter('humanizeInt')(scope.sdate - scope.ssdate) + " seconds";
                }

            })

          }
        }
      });
