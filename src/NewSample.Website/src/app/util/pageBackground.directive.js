'use strict';


angular.module('newsample')
    .directive('pageBackground', function ($state) {
        return {
          restrict: 'A',
          scope: {
            status: '='
          },

          link: function (scope, element, attrs) {
              scope.currentPage = $state;
              scope.$watch("currentPage.current.data.isSinglePage",function (isSinglePage) {
                  if (isSinglePage) {
                    element.addClass('gray-bg');
                  }
                  else {
                    element.removeClass('gray-bg');
                  }
              })
          }
        }
      });
