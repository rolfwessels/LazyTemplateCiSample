'use strict';


angular.module('newsample')
    .filter('asDate', function() {
        return function(input) {
          var date = new Date(input);
            return Math.floor(date.getTime() / 1000);
        };
    });
