'use strict';


angular.module('newsample')
    .filter('average', function() {
        return function(input) {
        	var sum = 0;
          angular.forEach(input,function (value) {
          	sum += value;
          });
          return  Math.round(sum / input.length*100)/100;
        };
    });
