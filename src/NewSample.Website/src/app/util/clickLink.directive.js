'use strict';


angular.module('newsample')
    .directive('clickLink', function ($location) {
        return {
          restrict: 'A',
          scope: {
            clickType: '=',
            clickValue: '='
          },
          
          link: function (scope, element, attrs) {
              if (attrs.clickLink === 'entry') {
               
                var uri = 'ui/entry/' + scope.clickValue.id;
                
                $(element).click(function() {
                  scope.$apply(function() {
                    $location.path(uri);
                  });
                  
                });
              }
          }
        };
      });



