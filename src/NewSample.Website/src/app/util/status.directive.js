'use strict';


angular.module('newsample')
    .directive('status', function () {
        return {
          restrict: 'A',
          scope: {
            status: '='
          },
          template: '<small class="label label--status" ng-class="{\'Failed\':\'label-danger\',\'Pending\':\'label-warning\',\'Success\':\'label-primary\',\'Running\':\'label-info\',\'Sending...\':\'label-info\',\'Sent\':\'label-primary\' }[status]" > {{status }} </small>',
          link: function (scope, element, attrs) {

          }
        }
      });
