'use strict';


angular.module('newsample')
    .directive('level', function () {
        return {
          restrict: 'A',
          scope: {
            level: '='
          },
          template: '<small class="label label--level" ng-class="{\'Error\':\'label-danger\',\'Warn\':\'label-warning\',\'Info\':\'label-primary\',\'Debug\':\'label-info\', }[level]" >{{level }} </small>',
          link: function (scope, element, attrs) {

          }
        }
      });
