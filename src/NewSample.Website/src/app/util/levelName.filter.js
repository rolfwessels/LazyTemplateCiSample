'use strict';


angular.module('newsample')
    .filter('levelName', function() {
        return function(input) {
          	switch (input) {
          		case 0: return "Debug";
          		case 1: return "Info";
          		case 2: return "Warn";
          		case 3: return "Error";
          	}
          	return input;
        };
    });
