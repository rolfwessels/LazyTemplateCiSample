/* apiEndPoint */

angular.module('newsample')
  .service('baseUrl', function(apiBase) {


    var apiUrl = apiBase || window.location.origin + window.location.pathname ;
    //work around for watcher
    if (apiUrl === "http://localhost:3000/") apiUrl =  "http://localhost:8081/";
    /*
     * Service
     */
    var baseUrlService =  {
       apiUrl : apiUrl+'api',
       tokenUrl : apiUrl + 'token',
       signalrUrl : apiUrl + 'signalr',
    };
    return baseUrlService;

  });
