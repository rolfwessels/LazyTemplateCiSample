'use strict';

angular.module('newsample')
  .controller('projectCtrl', function (apiProject,crudService ) {
    var self = this;
    var scope = angular.extend(this,{
      crud : crudService(apiProject)
    });
  });


      /* scaffolding [
          {
            "FileName": "app\\app.js",
            "Indexline": ".state('ui.project'",
            "InsertAbove": true,
            "Lines": [
              ".state('ui.project', {",
              "    url: '/project/:projectId',",
              "    templateUrl: 'app/project/projectDashboard.html',",
              "    data: { pageTitle: 'Project' }",
              "})"
            ]
          },
          {
            "FileName": "common\\navigation.html",
            "Indexline": "<!--AddAbove-->",
            "InsertAbove": true,
            "Lines": [
            '<li ui-sref-active="active">',
            '    <a ui-sref="ui.project"><i class="fa fa-lock"></i> <span class="nav-label">Project</span> </a>',
            '</li>',
            ]
          },
          {
            "FileName": "dashboard.controller.js",
            "Indexline": ",apiProject",
            "InsertInline": true,
            "Lines": [
              ",apiProject"
            ]
          },
          {
            "FileName": "dashboard.controller.js",
            "Indexline": "mapData(apiProject, 'Projects', '#/project');",
            "InsertAbove": false,
            "Lines": [
            "mapData(apiProject, 'Projects', '#/project');",
            ]
          }
      ] scaffolding */
