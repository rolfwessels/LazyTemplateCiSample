'use strict';

angular.module('newsample')
  .controller('userCtrl', function (apiUser,crudService ) {
    var self = this;
    var scope = angular.extend(this,{
      crud : crudService(apiUser)
    });
  });
