'use strict';

angular.module('newsample')
  .controller('notificationCtrl', function(apiChannel, apiDashboardItem, apiNotificationSchedule,apiQueue , apiNotificationLog, polling,$q ,$scope) {


    var scope = angular.extend(this, {
      //lookups
      channels: {},
      notifications: {},
      schedules: {},
      //other
      newNotification: {},
      showScheduleForm: false,
      recentQueue: [],
      notificationLog: [],
      notificationSchedules: [],

      refreshAll : function() {
        apiNotificationSchedule.getAll().then(function(data) {
          angular.copy(data, scope.notificationSchedules);
          scope.showScheduleForm = scope.notificationSchedules.length ==0;
        })
        scope.refreshQueue();
        scope.refreshLog();
      },
      edit: function(notification) {
        scope.newNotification = notification;
        scope.showScheduleForm = true;
      },
      nameFocus: function() {
        if (!scope.newNotification.name) {
          scope.newNotification.name = "";
          angular.forEach(scope.newNotification.notificationType, function(value, key) {
            var notif = scope.notifications[value];
            if (scope.newNotification.name) scope.newNotification.name += " and ";
            scope.newNotification.name += notif.name;
          });
        }
      },
      descriptionFocus: function() {
        if (!scope.newNotification.description && scope.newNotification.schedule) {
          scope.newNotification.description = "";
          angular.forEach(scope.newNotification.notificationType, function(value, key) {
            var notif = scope.notifications[value];
            if (scope.newNotification.description) scope.newNotification.description += " and ";
            scope.newNotification.description += notif.name;
          });
          scope.newNotification.description = "Notifications for "+scope.newNotification.description;

          scope.newNotification.description += " will be sent ";
          var schedule = scope.schedules[scope.newNotification.schedule];
          scope.newNotification.description += schedule.name;

        }
      },
      saveChannel: function(channel) {
        channel.disabled = true;
        apiChannel.validate(channel.key, channel.settings).then(function(response) {
          if (response.isValid) {
            apiChannel.setSettings(channel.key, channel.settings).then(function(response) {
              channel.showEdit = false;
              channel.errors = {};
              channel.disabled = false;
              angular.copy(response, channel);
            })
          } else {
            channel.errors = response.errors;
            channel.disabled = false;
          }
        })

      },
      deleteNotification: function(notification) {
        apiNotificationSchedule.delete(notification.id).then(function(response) {
          scope.showScheduleForm = false;
          angular.copy({}, notification);
          scope.refreshAll();

        },function (response) {
          alert(response);
        })

      },
      saveNotification: function(notification) {
        notification.disabled = true;
        var saveCall = notification.id? apiNotificationSchedule.post(notification.id,notification): apiNotificationSchedule.put(notification)
        saveCall.then(function(response) {
          notification.disabled = false;
          scope.showScheduleForm = false;
          angular.copy({}, notification);
          scope.refreshAll();
        },function (response,casd) {
          notification.errors = response.errors;
          notification.disabled = false;
        })
      },
      refreshQueue: function () {
        apiQueue.getAll().then(function(data) {
          angular.copy(data, scope.recentQueue);
        })
      },
      refreshLog: function () {
        apiNotificationLog.getAll().then(function(data) {
          angular.copy(data, scope.notificationLog);
        })
      }
    });

    apiChannel.getAll().then(function(data) {
      angular.copy(window.toMap(data),  scope.channels);
    })
    apiNotificationSchedule.getNotification().then(function(data) {
      angular.copy(window.toMap(data), scope.notifications);
    })
    apiNotificationSchedule.getSchedules().then(function(data) {
      angular.copy(window.toMap(data), scope.schedules);
    })

    var refreshQueue = new polling(function () {
      scope.refreshQueue();
      scope.refreshLog();
    },10000);
    refreshQueue.start($scope);

    scope.refreshAll();
  });
