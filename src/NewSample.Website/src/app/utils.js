'use strict';

(function(){
    window.lookupByKey = function(array,value,key) {
        return key;
    }
    window.toMap = function(array,key) {
        if (!key) key = "key";
        var results = {};
        for (var i = 0; i < array.length; i++) {
          var val = array[i]
          results[val[key]] = val;
        }
        return results;
    }
}());
