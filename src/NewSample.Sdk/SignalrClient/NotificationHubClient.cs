using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using NewSample.Sdk.SignalrClient.Base;
using NewSample.Shared.Models;
using Microsoft.AspNet.SignalR.Client;

namespace NewSample.Sdk.SignalrClient
{

    public class NotificationHubClient : BaseHubClient
    {
        public NotificationHubClient(HubConnection hubConnection) : base(hubConnection, "NotificationHub")
        {
            
        }

        public async Task Subscribe<T>(string name, Action<string,ValueUpdateModel<T>> callback)
        {
            _hub.On("OnUpdate", callback);
            await _hub.Invoke("SubscribeToUpdates", name);
        }

        public async Task Unsubscribe(string name)
        {
            await _hub.Invoke("UnsubscribeFromUpdates", name);
        }
    }
}