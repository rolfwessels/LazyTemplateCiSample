using NewSample.Sdk.RestApi.Base;
using NewSample.Shared;
using NewSample.Shared.Interfaces.Shared;
using NewSample.Shared.Models;
using NewSample.Shared.Models.Reference;

namespace NewSample.Sdk.RestApi
{
    public class ProjectApiClient : BaseCrudApiClient<ProjectModel, ProjectCreateUpdateModel, ProjectReferenceModel>,
        IProjectControllerActions
    {
        public ProjectApiClient(RestConnectionFactory restConnectionFactory)
            : base(restConnectionFactory, RouteHelper.ProjectController)
        {
        }

    }
}
