using System;
using System.Threading.Tasks;
using NewSample.Shared;
using NewSample.Shared.Interfaces.Base;
using NewSample.Shared.Models.Interfaces;
using NewSample.Utilities.Helpers;
using RestSharp;

namespace NewSample.Sdk.RestApi.Base
{
    public class BaseCrudApiClient<TModel, TDetailModel, TReferenceModel> : BaseGetApiClient<TModel, TReferenceModel>,
                                                                            ICrudController<TModel, TDetailModel>
        where TModel : IBaseModel, new()
    {
        protected BaseCrudApiClient(RestConnectionFactory restConnectionFactory, string userController)
            : base(restConnectionFactory, userController)
        {
        }

        #region ICrudController<TModel,TDetailModel> Members

        public async Task<TModel> GetById(string id)
        {
            RestRequest request = DefaultRequest(_apiPrefix.UriCombine(RouteHelper.WithId), Method.GET);
            request.AddUrlSegment("id", id.ToString());
            return await ExecuteAndValidate<TModel>(request);
        }

        public async Task<TModel> Insert(TDetailModel model)
        {
            RestRequest request = DefaultRequest(_apiPrefix, Method.POST);
            request.AddBody(model);
            return await ExecuteAndValidate<TModel>(request);
        }

        public async Task<TModel> Update(string id, TDetailModel model)
        {
            RestRequest request = DefaultRequest(_apiPrefix.UriCombine(RouteHelper.WithId), Method.PUT);
            request.AddUrlSegment("id", id);
            request.AddBody(model);
            return await ExecuteAndValidate<TModel>(request);
        }

        public async Task<bool> Delete(string id)
        {
            RestRequest request = DefaultRequest(_apiPrefix.UriCombine(RouteHelper.WithId), Method.DELETE);
            request.AddUrlSegment("id", id.ToString());
            return await ExecuteAndValidateBool(request);
        }

        #endregion
    }
}