using NewSample.Sdk.RestApi.Base;
using NewSample.Shared;
using NewSample.Shared.Interfaces.Shared;
using NewSample.Shared.Faction;
using NewSample.Shared.Faction.Reference;

namespace NewSample.Sdk.RestApi
{
    public class FactionApiClient : BaseCrudApiClient<FactionModel, FactionCreateUpdateModel, FactionReferenceModel>,
        IFactionControllerActions
    {
        public FactionApiClient(RestConnectionFactory restConnectionFactory)
            : base(restConnectionFactory, RouteHelper.FactionController)
        {
        }

    }
}
