using System.Reflection;

[assembly: AssemblyTitle("NewSample.Dal.Tests")]
[assembly: AssemblyDescription("Contains all NewSample unit tests")]
[assembly: NewSample.Utilities.Helpers.Log4NetInitialize("NewSample.Dal.Tests")]
[assembly: log4net.Config.XmlConfigurator]