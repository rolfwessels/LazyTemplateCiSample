using FizzWare.NBuilder;
using FizzWare.NBuilder.Generators;
using FluentValidation.TestHelper;
using NewSample.Core.Tests.Helpers;
using NewSample.Dal.Validation;
using NUnit.Framework;
using FluentAssertions;
using System.Linq;
using NewSample.Dal.Models;
using NewSample.Utilities.Helpers;

namespace NewSample.Dal.Tests.Validation
{
    [TestFixture]
    public class FactionValidatorTests
    {

        private FactionValidator _validator;

        #region Setup/Teardown

        public void Setup()
        {
            _validator = new FactionValidator();
        }

        [TearDown]
        public void TearDown()
        {

        }

        #endregion

        [Test]
        public void Validate_GiveValidFactionData_ShouldNotFail()
        {
            // arrange
            Setup();
            var faction = Builder<Faction>.CreateNew().WithValidData().Build();
            // action
            var validationResult = _validator.Validate(faction);
            // assert
            validationResult.Errors.Select(x => x.ErrorMessage).StringJoin().Should().BeEmpty();
            validationResult.IsValid.Should().BeTrue();
        }

         
        [Test]
        public void Name_GiveNullName_ShouldFail()
        {
            // arrange
            Setup();
            // assert
            _validator.ShouldHaveValidationErrorFor(faction => faction.Name, null as string);
        }

        [Test]
        public void Name_GiveLongString_ShouldFail()
        {
            // arrange
            Setup();
            // assert
            _validator.ShouldHaveValidationErrorFor(faction => faction.Name, GetRandom.String(200));
        }

        
         
    }
}